# OpenML dataset: IMDB_movie_1972-2019

https://www.openml.org/d/43603

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The IMDB Movies Dataset contains information about 5834 movies. Information about these movies was scraped from imdb for  purpose of creating a movie recommendation model. The data was preprocessed and cleaned to be ready for machine learning applications.
Content

Title
Year
Rating
Metascore
Votes
Description
Genre
Runtime (Minutes)
Revenue (Millions)
Actors
Director

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43603) of an [OpenML dataset](https://www.openml.org/d/43603). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43603/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43603/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43603/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

